# -*- coding: utf-8 -*-

from werkzeug import generate_password_hash, check_password_hash
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import ForeignKey, DateTime, Integer, String, Text, Boolean, Column
from sqlalchemy.orm import relationship
from datetime import datetime

Base = declarative_base()

class User(Base):
  __tablename__ = 'users'
  id = Column(Integer, primary_key=True)
  firstname = Column(String)
  lastname = Column(String)
  email = Column(String, unique=True)
  pwdhash = Column(String)

  def __init__(self, firstname, lastname, email, password):
    self.firstname = firstname.title()
    self.lastname = lastname.title()
    self.email = email.lower()
    self.set_password(password)

  def set_password(self, password):
    self.pwdhash = generate_password_hash(password)

  def check_password(self, password):
    return check_password_hash(self.pwdhash, password)

class Task(Base):
  __tablename__ = 'tasks'
  id = Column(Integer, primary_key=True)
  uid = Column(Integer, ForeignKey('users.id'))
  created_at = Column(DateTime, default=datetime.now)
  title = Column(String)
  description = Column(Text)
  priority = Column(String)
  done = Column(Integer, default=0)
  finished_at = Column(DateTime)
  #user = relationship(User, lazy='joined', join_depth=1, viewonly=True)