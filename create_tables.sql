CREATE TABLE users (
  id INTEGER PRIMARY KEY,
  firstname VARCHAR NOT NULL,
  lastname VARCHAR NOT NULL,
  email VARCHAR NOT NULL UNIQUE,
  pwdhash VARCHAR NOT NULL
);

CREATE TABLE tasks (
  id INTEGER PRIMARY KEY,
  uid INT NOT NULL,
  created_at DATE NOT NULL,
  title VARCHAR NOT NULL,
  description TEXT NOT NULL,
  priority VARCHAR NOT NULL,
  done INT,
  finished_at DATE
);