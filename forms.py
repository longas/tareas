# -*- coding: utf-8 -*-

from flask_wtf import Form, RecaptchaField
from wtforms import TextField, TextAreaField, SubmitField, PasswordField, SelectField, validators
from models import User

class LoginForm(Form):
  email = TextField("Email",  [validators.Required("Introduce tu email."), validators.Email("Introduce tu email.")])
  password = PasswordField(u"Contraseña", [validators.Required(u"Introduce una contraseña.")])
  submit = SubmitField("Entrar")

  def __init__(self, *args, **kwargs):
    Form.__init__(self, *args, **kwargs)

  def validate(self, db):
    if not Form.validate(self):
      return False

    user = db.session.query(User).filter_by(email = self.email.data.lower()).first()
    if user and user.check_password(self.password.data):
      return True
    elif not user:
      self.email.errors.append("Ese usuario no existe")
      return False
    else:
      self.email.errors.append(u"Email y/o contraseña incorrectos")
      return False

class RegisterForm(Form):
  firstname = TextField("Nombre",  [validators.Required("Introduce tu nombre.")])
  lastname = TextField("Apellido",  [validators.Required("Introduce tu apellido.")])
  email = TextField("Email",  [validators.Required("Introduce tu email."), validators.Email("Please enter your email address.")])
  password = PasswordField(u"Contraseña", [validators.Required(u"Introduce una contraseña.")])
  recaptcha = RecaptchaField()
  submit = SubmitField("Registrarse")

  def __init__(self, *args, **kwargs):
    Form.__init__(self, *args, **kwargs)

  def validate(self, db):
    if not Form.validate(self):
      return False

    user = db.session.query(User).filter_by(email = self.email.data.lower()).first()
    if user:
      self.email.errors.append("Ese email ya esta en uso.")
      return False
    else:
      return True

class ProfileForm(Form):
  firstname = TextField("Nombre")
  lastname = TextField("Apellido")
  password = PasswordField(u"Contraseña")
  submit = SubmitField("Guardar")

  def __init__(self, *args, **kwargs):
    Form.__init__(self, *args, **kwargs)

  def validate(self):
    if not Form.validate(self):
      return False
    else:
      return True

class AddtaskForm(Form):
  title = TextField(u"Título", [validators.Required("Introduce un título.")])
  description = TextAreaField(u"Descripción", [validators.Required("Introduce una descripción.")])
  priority = SelectField("Prioridad: ", choices=[("low", "Baja"), ("medium", "Media"), ("high", "Alta")], default=1)
  submit = SubmitField("Guardar")

  def __init__(self, *args, **kwargs):
    Form.__init__(self, *args, **kwargs)

  def validate(self):
    if not Form.validate(self):
      return False
    else:
      return True

class EdittaskForm(Form):
  title = TextField(u"Título")
  description = TextAreaField(u"Descripción")
  priority = SelectField("Prioridad: ", choices=[("low", "Baja"), ("medium", "Media"), ("high", "Alta")], default=1)
  submit = SubmitField("Guardar")

  def __init__(self, *args, **kwargs):
    Form.__init__(self, *args, **kwargs)

  def validate(self):
    if not Form.validate(self):
      return False
    else:
      return True