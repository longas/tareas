Web de tareas
=============
Gabriel Longás
--------------

### Instalación y configuración
* Instalamos virtualenv para poder crear entornos de desarrollo de Python (**$ sudo pip install virtualenv**)
* Creamos nuestro entorno (**$ virtualenv tareas**) y lo inicializamos (**$ . tareas/bin/activate**)
* Creamos una carpeta llamada app dentro de tareas y metemos ahí los archivos
* Instalamos las depencias (**$ pip install -r requirements.txt**)
* Dentro de la carpeta recien creada ejecutamos la aplicación (**$ python app.py**)
* Entramos a nuestro navegador y nos metemos en **localhost:5000**