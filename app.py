# -*- coding: utf-8 -*-

from flask import Flask, render_template, request, flash, session, url_for, redirect, abort
from forms import LoginForm, RegisterForm, ProfileForm, AddtaskForm, EdittaskForm
from models import Base, User, Task
from flask.ext.sqlalchemy import SQLAlchemy
from functools import wraps
from datetime import datetime
from conf import *

# Configuración App
app = Flask(__name__)
app.config.from_object(__name__)

# Configuración SQL Alchemy
db = SQLAlchemy(app)
db.Model = Base

# Decoradores
def login_required(f):
  @wraps(f)
  def decorated_function(*args, **kwargs):
    if 'email' not in session:
      return redirect(url_for('login', next=request.url))
    return f(*args, **kwargs)
  return decorated_function

def logged(f):
  @wraps(f)
  def decorated_function(*args, **kwargs):
    if 'email' in session:
      return redirect(url_for('tasks', next=request.url))
    return f(*args, **kwargs)
  return decorated_function

# Rutas
@app.route('/', methods=['GET', 'POST'])
def login():
  """Formulario de login"""

  if 'email' in session:
    session['id'] = db.session.query(User).filter_by(email=session['email']).first().id
    return redirect(url_for('tasks'))

  form = LoginForm()

  if request.method == 'POST' and form.validate(db):
    session['email'] = form.email.data
    session['id'] = db.session.query(User).filter_by(email=form.email.data).first().id
    return redirect(url_for('tasks'))

  return render_template('home.html', form=form)

@app.route('/users/register/', methods=['GET', 'POST'])
@logged
def register():
  """Formulario de registro"""

  form = RegisterForm(db=db)

  if request.method == 'POST' and form.validate(db):
    newuser = User(form.firstname.data, form.lastname.data, form.email.data, form.password.data)
    db.session.add(newuser)
    db.session.commit()

    session['email'] = newuser.email
    return redirect(url_for('login'))

  return render_template('register.html', form=form)

@app.route('/users/logout/')
@login_required
def logout():
  """Logout"""

  session.pop('email', None)
  session.pop('id', None)
  return redirect(url_for('login'))

@app.route('/users/delete/')
@login_required
def deleteuser():
  """Eliminar usuario de la bbdd"""

  user = db.session.query(User).filter_by(email=session['email']).first()
  db.session.delete(user)

  user_tasks = db.session.query(Task).filter_by(uid=session['id']);
  for task in user_tasks:
    db.session.delete(task)

  db.session.commit()
  return logout()

@app.route('/users/')
def users():
  """Lista de usuarios"""

  users = db.session.query(User).order_by(User.lastname.asc())
  return render_template('users.html', users=users)

@app.route('/user/profile/', methods=['GET', 'POST'])
@login_required
def profile():
  """Perfil del usuario"""

  form = ProfileForm()

  if request.method == 'POST' and form.validate():
    user = db.session.query(User).filter_by(email=session['email']).first()

    if form.firstname.data:
      user.firstname = form.firstname.data
    if form.lastname.data:
      user.lastname = form.lastname.data
    if form.password.data:
      user.set_password(form.password.data)

    db.session.commit()

    return redirect(url_for('tasks'))

  return render_template('profile.html', form=form)

@app.route('/about/')
def about():
  """Página about"""
  return render_template('about.html')

@app.route('/tasks/')
@login_required
def tasks():
  """Lista de tareas"""

  tasks = {}
  tasks_todo = db.session.query(Task).filter_by(uid=session['id']).filter_by(done=0).order_by(Task.created_at.asc())
  tasks_done = db.session.query(Task).filter_by(uid=session['id']).filter_by(done=1).order_by(Task.finished_at.asc())
  tasks['todo'] = tasks_todo
  tasks['done'] = tasks_done

  return render_template('tasks.html', tasks=tasks)

@app.route('/tasks/add/', methods=['GET', 'POST'])
@login_required
def addtask():
  """Añadir tareas"""

  form = AddtaskForm(request.form)

  if request.method == 'POST' and form.validate():
    task = Task(uid=session['id'])
    form.populate_obj(task)
    db.session.add(task)
    db.session.commit()
    return redirect(url_for('tasks'))

  return render_template('addtask.html', form=form)

@app.route('/tasks/<int:task_id>/')
@login_required
def taskdetail(task_id):
  """Detalles de una tarea"""

  task = db.session.query(Task).get(task_id)

  if task is None or task.uid != session['id']:
    abort(404)

  return render_template('taskdetail.html', task=task)

@app.route('/tasks/done/<int:task_id>/')
@login_required
def taskdone(task_id):
  """Marcar hecha una tarea"""

  task = db.session.query(Task).get(task_id)

  if task is None or task.uid != session['id']:
    abort(404)

  task.done = 1
  task.finished_at = datetime.now();
  db.session.commit()
  return redirect(url_for('tasks'))

@app.route('/tasks/delete/<int:task_id>/')
@login_required
def taskdelete(task_id):
  """Borrar tarea"""

  task = db.session.query(Task).get(task_id)

  if task is None or task.uid != session['id']:
    abort(404)

  db.session.delete(task)
  db.session.commit()
  return redirect(url_for('tasks'))

@app.route('/tasks/edit/<int:task_id>/', methods=['GET', 'POST'])
@login_required
def taskedit(task_id):
  """Editar tarea"""

  form = EdittaskForm()

  if request.method == 'POST' and form.validate():
    task = db.session.query(Task).get(task_id)

    if form.title.data:
      task.title = form.title.data
    if form.description.data:
      task.description = form.description.data
    if form.priority.data:
      task.priority = form.priority.data

    db.session.commit()

    return redirect(url_for('tasks'))

  return render_template('edittask.html', form=form)

# Manejo de errores
@app.errorhandler(404)
def notfound(e):
  return render_template('404.html'), 404

@app.errorhandler(403)
def forbidden(e):
  return render_template('403.html'), 403

# Lanzamos el servidor
if __name__ == '__main__':
  app.run(debug=True)