# -*- coding: utf-8 -*-

# Clave para formularios
SECRET_KEY = "clave secreta"

# Conexión a la bbdd
SQLALCHEMY_DATABASE_URI = 'sqlite:///database.db'

# Recaptcha
RECAPTCHA_USE_SSL = False
RECAPTCHA_PUBLIC_KEY = '6Ld4ReoSAAAAAN1ajNQnVRXwizcc7ZCcLQK354Oe'
RECAPTCHA_PRIVATE_KEY = '6Ld4ReoSAAAAAKYR39iaLoMDGoScGOIgPizpfevs'
RECAPTCHA_OPTIONS = {'theme': 'white'}